import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  customersCollection:AngularFirestoreCollection;  
  
  public getBooks(userId: string){
    this.customersCollection = this.db.collection(`users/${userId}/customers`);
    return this.customersCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }
  public deleteBook(userId:string , id:string){
    this.db.doc(`users/${userId}/customers/${id}`).delete();
  }
  updateBook(userId:string,id:string,name:string,income:string, nyears:string){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        income:income,
        nyears:nyears
      }
    )
  }

  constructor(private db:AngularFirestore) { }

}