import { HighContrastModeDetector } from '@angular/cdk/a11y';
import { Component, OnInit } from '@angular/core';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { Doctor } from 'src/app/interfaces/doctor';
import { User } from '../interfaces/user';
import { UploadService } from '../upload.service';


@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.css']
})
export class FileuploadComponent implements OnInit {

  fileObj: File;
  fileUrl: string;
  errorMsg: boolean;
  title = 's3-file-uploader-app';
  doctor:Doctor[];
  user:User[]
  similarity:number;
  flowername:string;
  confidence:number;
  flower2:number;
  flower3:number;


  constructor(private fileUploadService: UploadService) {
    this.errorMsg = false;
  }
  ngOnInit(): void {
    // throw new Error("Method not implemented.");
  }
  refresh(): void {
    this.doctor=null;
   }

   onFilePicked(event: Event): void {

    this.errorMsg = false
    console.log(event);
    const FILE = (event.target as HTMLInputElement).files[0];
    this.fileObj = FILE;
    console.log(this.fileObj);
  }
  onFileUpload(event: Event) {
    this.errorMsg = false
    console.log(event);
    const FILE = (event.target as HTMLInputElement).files[0];
    this.fileObj = FILE;
    console.log(this.fileObj);
    
    
    if (!this.fileObj) {
      this.errorMsg = true
      return
    }
    const fileForm = new FormData();
    fileForm.append('img', this.fileObj);
    this.fileUploadService.fileUpload(fileForm).subscribe(res => {
    this.fileUrl = 'https://my-upload-adiel.s3.amazonaws.com/' + res['imgName'];
    console.log(this.fileUrl)
    this.doctor=res;
    console.log(res)
    this.flowername = res['response'][0]['Name']
    this.confidence = res['response'][0]['Confidence']
    this.flower2 = res['response'][1]['Name']
    this.flower3 = res['response'][2]['Name']
    console.log(this.flowername)
    console.log(this.confidence)
    console.log(this.flower2)
    console.log(this.flower3)
    
    });
  }


  
}