export interface Doctor {
    info?:string,
    kind?:string,
    date?:Date,
    city?:string,
    name?:string,
}