export interface Book {
    name: string;
    nyears: string;
    income: string;
    id?:string;
    saved?:Boolean;
    result?:string;
}

