
import { Patient } from './../interfaces/patient';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../auth.service';
import { HeartService } from '../heart.service';
import { Router } from '@angular/router';
import { MatTable } from '@angular/material/table';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
 
  flowers = [];
  fileObj: File;
  fileUrl: string;
  errorMsg: boolean;
  title = 's3-file-uploader-app';
  patients$;
  patients: Patient[];
  userId: string;
  @ViewChild(MatTable,{static:true}) table: MatTable<any>;
  sexDict = {0:'Female',1:'Male'};

  /* displayedColumns: string[] = ['name','age', 'sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg'
    , 'thalach', 'exang', 'oldpeak', 'slope', 'ca', 'thal', 'Delete']; */

  displayedColumns: string[] = ['flowerinfo','flowerkind', 'date','city'];
  name:string;
  info:string;
  kind:string;
  city:string;
  


  deletePatient(name,i) {
   
   return this.heartService.deletePatient(name).toPromise().then(()=>{
    this.patients.splice(i,1);
    this.table.renderRows();
      console.log("success")
      
    });
  }

  redirect() {
    this.router.navigate(['/wellcome']);
  }

  

  constructor(private router:Router, private heartService: HeartService,public authService: AuthService) { 
      this.errorMsg = false;
    }

  ngOnInit(): void {
    this.heartService.getPatient().subscribe(
      res => {
        console.log(res);
        this.flowers = res;
        console.log(this.flowers);
        
      }
       
    )
  }

}