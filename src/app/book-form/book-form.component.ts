import { Book } from './../interfaces/book';
import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';



@Component({
  selector: 'bookform',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {
  @Input() name:string;
  @Input() income:string;
  @Input() id:string; 
  @Input() nyears:string; 
  @Output() closeEdit = new EventEmitter<null>();
  
  
  @Output() update = new EventEmitter<Book>();
  
  
  updateParent(){
    let book:Book = {id:this.id, name:this.name, income:this.income, nyears:this.nyears};
    this.update.emit(book); 
  }
  tellParentToClose(){
    this.closeEdit.emit(); 
  }



  constructor() { }

  ngOnInit(): void {
  }

}
