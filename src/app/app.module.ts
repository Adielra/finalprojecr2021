
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AngularFireAuthModule } from '@angular/fire/auth';
import{ AngularFirestoreModule} from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { environment } from './../environments/environment.prod';
import { LoginComponent } from './login/login.component';
import {MatInputModule} from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import { SignUpComponent } from './sign-up/sign-up.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { BookFormComponent } from './book-form/book-form.component';
import { FileuploadComponent } from './fileupload/fileupload.component';
import { WellcomeComponent } from './wellcome/wellcome.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { ListComponent } from './list/list.component';
import {  ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MatTableModule} from '@angular/material/table';



@NgModule({
  declarations: [
    
    AppComponent,
    NavComponent,
    LoginComponent,
    SignUpComponent,
    BookFormComponent,
    FileuploadComponent,
    WellcomeComponent,
    AppointmentsComponent,
    ListComponent,
    
  ],
  imports: [
    MatDatepickerModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatTableModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatStepperModule,
    HttpClientModule,
    MatProgressBarModule,
    MatTooltipModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatListModule,
    MatInputModule,
    FormsModule,
    MatSelectModule,
    MatExpansionModule,
    MatCardModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
