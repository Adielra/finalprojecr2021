import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';  



@Injectable({
  providedIn: 'root'
})

export class UploadService {

  constructor(private http: HttpClient) { }
  
  
  fileUpload(file: FormData):Observable<any> {
  return this.http.post('http://ec2-54-86-151-99.compute-1.amazonaws.com/upload_image', file);
  }


  // fileUpload(file: FormData) {
  //   console.log("אלירן")
  //    console.log(this.http.get<any>('http://ec2-3-84-231-193.compute-1.amazonaws.com/useRekognition'));
  //    return this.http.get<any>('http://ec2-3-84-231-193.compute-1.amazonaws.com/useRekognition');
  // }

  // getComareasionn() {  
  //   return this.http.get<any>('http://xxx.xxx.4.xxx:9200/pcn/_search');  
  //   }  


}