import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from './../auth.service';
import { Router,ActivatedRoute } from '@angular/router';


import { Location } from "@angular/common";




@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

  title: string = 'Books';
  useremail:string;
  userId:string; 


  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  

    logout(){
      this.authService.logout()
      this.router.navigate(['/wellcome']);;
    }
   
constructor(private breakpointObserver: BreakpointObserver, public authService:AuthService,
  private router:Router,location: Location) {
  router.events.subscribe(val => {

    if (location.path() == "/books" || location.path() == "/bookform") {
    this.title = 'Books';      
    } else if (location.path() == "/signup"){
    this.title = "Sign up form";
    } else if (location.path() == "/login"){
    this.title = "Login form";
    
    } else if (location.path() == "/application"){
    this.title = "application Form";
    
    }
    });   
    


}
ngOnInit() {
  this.authService.user.subscribe(
    user => {
      this.userId = user.uid;
      this.useremail = user.email;
    }
    )
  }

}
