import { Book } from './../interfaces/book';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  userId:string;
  books$;
  book:Book[];
  panelOpenState = false;
  editstate = [];
    

  constructor(public authService:AuthService, private booksService:BooksService) { }
  
  public deleteBook(id:string){
    this.booksService.deleteBook(this.userId,id);
  }
  update(book:Book){
    this.booksService.updateBook(this.userId, book.id ,book.name, book.income, book.nyears);
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.books$ = this.booksService.getBooks(this.userId);
      }
    )
  }
  Classify(){}

}
