import { HeartService } from './../heart.service';
import { AuthService } from 'src/app/auth.service';
import { AppointmentsService } from 'src/app/appointments.service';
import { Component, OnInit } from '@angular/core';
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.css']
})
export class AppointmentsComponent implements OnInit {

  positionOptions: TooltipPosition[] = ['above','left'];
  position = new FormControl(this.positionOptions[0]);
  category:string;
  typeOfTreatment:string;
  date:string;
  city:string;
  time:string;
  userId:string;
  id:string;
  isEdit:boolean = false;

  flowerkind;
  flowerinfo;

  
  constructor(public appointmentsService:AppointmentsService,
    private router:Router,private authService:AuthService,
    private route:ActivatedRoute,
    private heartservice:HeartService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.user.subscribe(
      user=> {
        this.userId = user.uid;
      }
    )
  }

  addAppointment(){
    
    console.log("addAppointment")
    console.log(this.flowerkind)
    console.log(this.flowerinfo)
    console.log(this.date)
    console.log(this.city)
    console.log(5)
    this.heartservice.addflower( this.flowerkind,this.flowerinfo,this.date,this.city).subscribe(
      res => {
       
        console.log(5);
        this.router.navigate(['/list']);
        
      }
       
    )
    
    
   }
  

}
