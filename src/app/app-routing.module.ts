import { WellcomeComponent } from './wellcome/wellcome.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import {  FileuploadComponent } from './fileupload/fileupload.component';
import { BooksComponent } from './books/books.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { BookFormComponent } from './book-form/book-form.component';
import { ListComponent } from './list/list.component';


const routes: Routes = [
  { path: 'application', component: AppointmentsComponent},
  { path: 'login', component: LoginComponent},
  { path: 'fileupload', component: FileuploadComponent},
  { path: 'books', component: BooksComponent},
  { path: 'booksform', component: BookFormComponent},
  { path: 'list', component: ListComponent},
  { path: 'signup', component: SignUpComponent},
  { path: 'wellcome', component: WellcomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
